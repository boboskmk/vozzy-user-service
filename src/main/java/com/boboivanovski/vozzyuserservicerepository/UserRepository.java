package com.boboivanovski.vozzyuserservicerepository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.boboivanovski.vozzyuserservice.model.User;

public interface UserRepository extends MongoRepository<User, String> {

    User findUserByEmail(String email);

    //TODO: User findByRole_Role(String role);

    User findUserById(String id);

    //TODO: User findUserByToken(String token);
    
    User findUserByUsername(String username);
}
