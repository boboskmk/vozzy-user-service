package com.boboivanovski.vozzyuserservice.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.boboivanovski.vozzyuserservice.model.ResetPassword;
import com.boboivanovski.vozzyuserservice.model.User;
import com.boboivanovski.vozzyuserservicerepository.UserRepository;


@Component
public class Validation implements Validator {

    @Value("${account.password.forgot.messages.not-match}")
    private String notMatchMessage;

    @Value("${account.password.forgot.messages.not-empty}")
    private String notEmptyMessage;

    @Autowired
    private UserRepository userRepository;

    private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    
    /* VALID_PASSWORD_REGEX :
     	^                 # start-of-string
		(?=.*[0-9])       # a digit must occur at least once
		(?=.*[a-z])       # a lower case letter must occur at least once
		(?=.*[A-Z])       # an upper case letter must occur at least once
		(?=\S+$)          # no whitespace allowed in the entire string
		.{6,16}           # anything, at least six places though to sixteen
		$                 # end-of-string
     */
    private static final Pattern VALID_PASSWORD_REGEX = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,16}$");



    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ResetPassword resetPassword = (ResetPassword) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newPassword", notEmptyMessage);
        if (!resetPassword.getNewPassword().equals(resetPassword.getRepeatPassword())) {
            errors.rejectValue("repeatPassword", notMatchMessage);
            return;
        }
    }

    public boolean checkIfEmailExistsInDb(String email) {

        User user = userRepository.findUserByEmail(email);

        return user != null;

    }

    /**
     * Validate email
     */
    public boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    /**
     * Validate password length (should be between 6-16 characters)
     */
    public boolean validatePassword(String password){
        Matcher matcher = VALID_PASSWORD_REGEX.matcher(password);
        return matcher.find();
    }
    
    public boolean validateFirstName(String firstName) {
    	return (firstName != null && !firstName.isEmpty());
    }
    
    public boolean validateLastName(String lastName) {
    	return (lastName != null && !lastName.isEmpty());
    }
}