package com.boboivanovski.vozzyuserservice.model;

public class ResetPassword {

    private String newPassword;

    private String repeatPassword;

    private String token;

    protected ResetPassword(){};

    public ResetPassword(String token){
        this.token = token;
    };

    public ResetPassword(String newPassword, String repeatPassword, String token) {
        this.newPassword = newPassword;
        this.repeatPassword = repeatPassword;
        this.token = token;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
