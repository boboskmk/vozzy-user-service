package com.boboivanovski.vozzyuserservice.model;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

@Document(collection = "user")
public class User {

    @Id
    private String id;

    private String email;

    private String password;

    private String firstName;
    
    private String lastName;
    
    private Integer active = 1; // enable/disable profile 

    private Role role;
    
    private List<LicensePlate> licensePlates;
    
    @JsonProperty
    private boolean isEnabled = false; // is email valid
    
    private Long dateOfCreation; // miliseconds since Epoch

	public User() {
	}
	
	public User(String email, String password, String firstName, String lastName, Role role) {
	
		this.email = email;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfCreation = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC) * 1000;
		this.role = role;
		this.licensePlates = new ArrayList<LicensePlate>();
	}

	public User(String email, String password, String firstName, String lastName, Integer active,
			boolean isEnabled, Long dateOfCreation, Role role) {
	
		this.email = email;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.active = active;
		this.isEnabled = isEnabled;
		this.dateOfCreation = dateOfCreation;
		this.role = role;
		this.licensePlates = new ArrayList<LicensePlate>();
	}
	

	public String getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public boolean isEnabled() {
		return isEnabled;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public Long getDateOfCreation() {
		return dateOfCreation;
	}

	public void setDateOfCreation(Long dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<LicensePlate> getLicensePlates() {
		return licensePlates;
	}

	public void setLicensePlates(List<LicensePlate> licensePlates) {
		this.licensePlates = licensePlates;
	}
	
	public void addLicensePlate(LicensePlate newLicensePlate) {
		this.licensePlates.add(newLicensePlate);
	}
	
	public void removeLicensePlate(LicensePlate licensePlateToRemove) {
		this.licensePlates.remove(licensePlateToRemove);
	}
	
	

    //TODO: add token
}
