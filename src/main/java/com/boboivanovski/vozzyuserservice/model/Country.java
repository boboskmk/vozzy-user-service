package com.boboivanovski.vozzyuserservice.model;

public class Country {
	
	private String name;
	
	private String licensePlateCode;
	
    private String flagImage;
    
    public Country() {
    	
    }
    
    

	public Country(String name, String licensePlateCode, String flagImage) {
		this.name = name;
		this.licensePlateCode = licensePlateCode;
		this.flagImage = flagImage;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLicensePlateCode() {
		return licensePlateCode;
	}

	public void setLicensePlateCode(String licensePlateCode) {
		this.licensePlateCode = licensePlateCode;
	}

	public String getFlagImage() {
		return flagImage;
	}

	public void setFlagImage(String flagImage) {
		this.flagImage = flagImage;
	}
    
    

}
