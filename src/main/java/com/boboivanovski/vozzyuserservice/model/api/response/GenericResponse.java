package com.boboivanovski.vozzyuserservice.model.api.response;

import org.springframework.http.HttpStatus;

public class GenericResponse {

    private HttpStatus httpStatus;

    private String message;

    protected GenericResponse(){}

    public GenericResponse(HttpStatus httpStatus, String message) {
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
