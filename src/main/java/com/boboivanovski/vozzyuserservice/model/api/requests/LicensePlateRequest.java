package com.boboivanovski.vozzyuserservice.model.api.requests;

import com.boboivanovski.vozzyuserservice.model.LicensePlate;

public class LicensePlateRequest {
	
	private String userId;
	
	private LicensePlate licensePlate;
	
	public LicensePlateRequest() {
		
	}

	public LicensePlateRequest(String userId, LicensePlate licensePlate) {
		this.userId = userId;
		this.licensePlate = licensePlate;
	}

	public String getUserId() {
		return userId;
	}

	public LicensePlate getLicensePlate() {
		return licensePlate;
	}
	
	
	
	

}
