package com.boboivanovski.vozzyuserservice.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

public class LicensePlate {
	
	
	
	private String plateNumber;
	
	private Country country;
	
	
	public LicensePlate() {
		
	}


	public LicensePlate(String plateNumber, Country country) {
		this.plateNumber = plateNumber;
		this.country = country;
	}


	public String getPlateNumber() {
		return plateNumber;
	}


	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}


	public Country getCountry() {
		return country;
	}


	public void setCountry(Country country) {
		this.country = country;
	}



	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}


	@Override
	public boolean equals(Object obj) {
		if (obj == this)
	        return true;
	    if (!(obj instanceof LicensePlate))
	        return false;
	    
	    LicensePlate other = (LicensePlate) obj;
	    
	    return (this.plateNumber.equals(other.getPlateNumber())) && (this.country.getName().equals(other.getCountry().getName()));
	}
	
	
	
	
	

}
