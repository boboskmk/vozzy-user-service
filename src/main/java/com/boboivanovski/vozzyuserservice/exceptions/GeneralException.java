package com.boboivanovski.vozzyuserservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.GATEWAY_TIMEOUT)
public class GeneralException extends RuntimeException {

    public GeneralException(String message){ super(message);}
}
