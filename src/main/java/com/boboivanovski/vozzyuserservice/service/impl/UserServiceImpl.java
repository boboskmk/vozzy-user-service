package com.boboivanovski.vozzyuserservice.service.impl;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.boboivanovski.vozzyuserservice.exceptions.BadRequestException;
import com.boboivanovski.vozzyuserservice.exceptions.ConflictException;
import com.boboivanovski.vozzyuserservice.exceptions.GeneralException;
import com.boboivanovski.vozzyuserservice.exceptions.RecordNotFoundException;
import com.boboivanovski.vozzyuserservice.exceptions.ServiceException;
import com.boboivanovski.vozzyuserservice.model.Role;
import com.boboivanovski.vozzyuserservice.model.Roles;
import com.boboivanovski.vozzyuserservice.model.UpdatePassword;
import com.boboivanovski.vozzyuserservice.model.User;
import com.boboivanovski.vozzyuserservice.model.api.requests.LicensePlateRequest;
import com.boboivanovski.vozzyuserservice.model.api.requests.NewUserRequest;
import com.boboivanovski.vozzyuserservice.model.api.response.GenericResponse;
import com.boboivanovski.vozzyuserservice.service.UserService;
import com.boboivanovski.vozzyuserservice.validator.Validation;
import com.boboivanovski.vozzyuserservicerepository.UserRepository;

import reactor.core.publisher.Mono;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private WebClient.Builder webClientBuilder;

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private Validation validation;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    

    @Value("${role.admin}")
    private String adminRole;

    @Value("${role.customer}")
    private String customerRole;
    
	@Value("${services.url.callplate-service}")
	private String callPlateServiceUrl;
    
    
    
	@Override
	public User findUserByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User findUserByUsername(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User findUserByEmail(String email) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User findUserByToken(String token) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User findUserById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> getAllUsers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User createAdmin(NewUserRequest user) {
		return createUser(user, Roles.ADMIN);
	}

	@Override
	public User createCustomer(NewUserRequest user) {
		return createUser(user, Roles.CUSTOMER);
	}

	@Override
	public User forgotPassword(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User updatePassword(UpdatePassword updatePassword) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User saveUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericResponse deleteUser(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericResponse deleteAdmin(String placeId) {
		// TODO Auto-generated method stub
		return null;
	}
	
    /**
     * General method for creating users
     */
    private User createUser(NewUserRequest user, Roles role) {

        /*Validate email*/
		if (!validation.validateEmail(user.getEmail())) {
			throw new BadRequestException("Email not valid");
		}

        /*Validate password length*/
        if(!validation.validatePassword(user.getPassword())){
            throw new BadRequestException("Password must be in range of 6-16 characters, must have at least one upper case, lower case and digit");
        }
        
        if(!validation.validateFirstName(user.getFirstName())){
            throw new BadRequestException("Firstname cannot be empty");
        }
        
        if(!validation.validateLastName(user.getLastName())){
            throw new BadRequestException("Lastname cannot be empty");
        }
        
        if (validation.checkIfEmailExistsInDb(user.getEmail())) {
        	 throw new ConflictException("Email already exists");
        }

        /*Creating USER according its role*/

        String decideRole = returnRole(role);
        
		User newUser = new User(user.getEmail(), passwordEncoder.encode(user.getPassword()), user.getFirstName(),
				user.getLastName(), new Role(role.getValue(), decideRole));
		
        final User newCustomerFromDb = userRepository.insert(newUser);
        
        return newCustomerFromDb;
    }
    
    private String returnRole(Roles role){
        if(Roles.ADMIN.equals(role)) return adminRole;
        if(Roles.CUSTOMER.equals(role)) return customerRole;
        return null;
    }

	@Override
	public GenericResponse addLicensePlate(LicensePlateRequest request) {
		 
		checkLicensePlateRequest(request);
		
		final User user = getUserById(request.getUserId());
		
		if (user.getLicensePlates() != null && user.getLicensePlates().contains(request.getLicensePlate())) {
			throw new ConflictException("License plate exists");
		}
		
		try {
			
			user.addLicensePlate(request.getLicensePlate());
			
			userRepository.save(user);
			
			// send new license plate request to Call Plate service
			webClientBuilder.build().post().uri(callPlateServiceUrl + "license-plate")
					.contentType(MediaType.APPLICATION_JSON)
					.header(HttpHeaders.AUTHORIZATION, "")
					.acceptCharset(Charset.forName("UTF-8"))
					.body(Mono.just(request), LicensePlateRequest.class).retrieve()
					.onStatus(HttpStatus::is5xxServerError, clientResponse -> {
						if (clientResponse.equals(HttpStatus.SERVICE_UNAVAILABLE)) {
							return Mono.error(new ServiceException("CallPlate-service is taking too long or is down"));
						}
						return Mono.error(new GeneralException("Connection refused. CallPlate-service is not reachable"));
					})
					.onStatus(HttpStatus::is4xxClientError,
							clientResponse -> Mono.error(new ConflictException("User already exist")))
					.bodyToMono(Void.class);
			
			return new GenericResponse(HttpStatus.OK, "License plate was successfully added");
			
		} catch (Exception e) { 
			/* Rollback user object */
			
			user.removeLicensePlate(request.getLicensePlate());
			
			userRepository.save(user);
			
			return new GenericResponse(HttpStatus.INTERNAL_SERVER_ERROR, "License plate was not successfully added");
			
		}

	}

	@Override
	public GenericResponse removeLicensePlate(LicensePlateRequest request) {
		
		checkLicensePlateRequest(request);
		
		final User user = getUserById(request.getUserId());
		
		if (user.getLicensePlates() != null && !user.getLicensePlates().contains(request.getLicensePlate())) {
			throw new RecordNotFoundException("License plate does not exists");
		}
		
		try {
			
			user.removeLicensePlate(request.getLicensePlate());
			
			userRepository.save(user);
			
			// send new license plate request to Call Plate service
			webClientBuilder.build().post().uri(callPlateServiceUrl + "license-plate")
					.contentType(MediaType.APPLICATION_JSON)
					.header(HttpHeaders.AUTHORIZATION, "")
					.acceptCharset(Charset.forName("UTF-8"))
					.body(Mono.just(request), LicensePlateRequest.class).retrieve()
					.onStatus(HttpStatus::is5xxServerError, clientResponse -> {
						if (clientResponse.equals(HttpStatus.SERVICE_UNAVAILABLE)) {
							return Mono.error(new ServiceException("CallPlate-service is taking too long or is down"));
						}
						return Mono.error(new GeneralException("Connection refused. CallPlate-service is not reachable"));
					})
					.onStatus(HttpStatus::is4xxClientError,
							clientResponse -> Mono.error(new ConflictException("User already exist")))
					.bodyToMono(Void.class);		
			
			return new GenericResponse(HttpStatus.OK, "License plate was successfully removed");
			
		} catch (Exception e) { 
			/* Rollback user object */
			
			user.addLicensePlate(request.getLicensePlate());
			
			userRepository.save(user);
			
			return new GenericResponse(HttpStatus.INTERNAL_SERVER_ERROR, "License plate was not successfully removed");
			
		}
		

	}

	private void checkLicensePlateRequest(LicensePlateRequest request) {
		if (request.getLicensePlate() == null) {
			throw new BadRequestException("License plate cannot be empty");
		}
		
		if (request.getUserId() == null || request.getUserId().isEmpty()) {
			throw new BadRequestException("User ID cannot be empty");
		}
	}    
	
	private User getUserById(String userId) {
		
		Optional<User> userOpt  = userRepository.findById(userId);
		
		if (userOpt.isEmpty()) {
			throw new RecordNotFoundException("user not found");
		}
		
		return userOpt.get();
	}

}
