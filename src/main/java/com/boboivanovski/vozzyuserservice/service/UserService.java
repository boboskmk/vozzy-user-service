package com.boboivanovski.vozzyuserservice.service;

import java.util.List;

import com.boboivanovski.vozzyuserservice.model.UpdatePassword;
import com.boboivanovski.vozzyuserservice.model.User;
import com.boboivanovski.vozzyuserservice.model.api.requests.LicensePlateRequest;
import com.boboivanovski.vozzyuserservice.model.api.requests.NewUserRequest;
import com.boboivanovski.vozzyuserservice.model.api.response.GenericResponse;

public interface UserService {

    User findUserByName(String name);
    
    User findUserByUsername(String username);

    User findUserByEmail(String email);

    User findUserByToken(String token);

    User findUserById(String id);

    List<User> getAllUsers();

    User createAdmin(NewUserRequest user);

    User createCustomer(NewUserRequest user);

    User forgotPassword(User user);

    User updatePassword(UpdatePassword updatePassword);

    User saveUser(User user);

    GenericResponse deleteUser(String id);

    GenericResponse deleteAdmin(String placeId);
    
    GenericResponse addLicensePlate(LicensePlateRequest request);
    
    GenericResponse removeLicensePlate(LicensePlateRequest request);
    
}
