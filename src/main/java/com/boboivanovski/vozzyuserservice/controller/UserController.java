package com.boboivanovski.vozzyuserservice.controller;

import javax.servlet.UnavailableException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boboivanovski.vozzyuserservice.exceptions.BadRequestException;
import com.boboivanovski.vozzyuserservice.exceptions.ConflictException;
import com.boboivanovski.vozzyuserservice.model.User;
import com.boboivanovski.vozzyuserservice.model.api.requests.NewUserRequest;
import com.boboivanovski.vozzyuserservice.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	
    @Autowired
    private UserService userService;
    
    @PostMapping("/admin")
//	@PreAuthorize("hasRole('ROLE_SUPERUSER')")
    public ResponseEntity<User> createAdmin(@RequestBody NewUserRequest request)  throws ConflictException, BadRequestException{
    	
        if (request.getEmail() == null || request.getEmail().isEmpty()) {
            throw new BadRequestException("Email value must be entered");
        }
        
        User user = userService.createAdmin(request);
        return ResponseEntity.ok(user);
    }
    
    @PostMapping("/customer")
    public ResponseEntity<User> createCustomer(@RequestBody NewUserRequest request) throws ConflictException, BadRequestException, UnavailableException {

        if (request.getEmail() == null || request.getEmail().isEmpty()) {
            throw new BadRequestException("Email value must be entered");
        }
        
        User user = userService.createCustomer(request);
        return ResponseEntity.ok(user);
    }
    
    //TODO: update password
    
    //TODO: add license plate
    
    //TODO: remove license plate
    
    
    
}
